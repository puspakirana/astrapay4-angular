import { Component} from '@angular/core';
import { faTableColumns, faGauge, faGears, faBook } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  faTableColumns = faTableColumns
  faGauge = faGauge
  faGears = faGears
  faBook = faBook
}
