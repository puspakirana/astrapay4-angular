import { Component, Input, OnInit } from '@angular/core';
import { UserAP } from 'src/app/interfaces/user.interface';

@Component({
  selector: 'app-table-dashboard',
  templateUrl: './table-dashboard.component.html',
  styleUrls: ['./table-dashboard.component.scss']
})
export class TableDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  userDetail: UserAP = {
    id: 0,
    username: "",
    name: "",
    email: "",
    dob: "",
    photo: "",
    wallet: {
        saldo: 0
    },
    role: {
        role: ""
    },
    memberList: []
  }

  @Input() dataResponse: UserAP[] = []

  getDetail(detail: UserAP) {
    console.log(detail)
    this.userDetail = detail
  }

}
