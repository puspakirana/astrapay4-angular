import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RouterModule } from '@angular/router';
import { ContentComponent } from './content/content.component';
import { HeadComponent } from './head/head.component';
import { FooterComponent } from './footer/footer.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HeadContentComponent } from './head-content/head-content.component';
import { TableDashboardComponent } from './table-dashboard/table-dashboard.component';



@NgModule({
  declarations: [
    NavbarComponent,
    SidebarComponent,
    ContentComponent,
    HeadComponent,
    FooterComponent,
    HeadContentComponent,
    TableDashboardComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FontAwesomeModule
  ],
  exports: [
    NavbarComponent,
    SidebarComponent,
    ContentComponent,
    HeadComponent,
    FooterComponent,
    HeadContentComponent,
    TableDashboardComponent
  ]
})
export class LayoutsModule { }
