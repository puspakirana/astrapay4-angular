import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable } from 'rxjs';
import { ResponseBase } from '../interfaces/response.interface';
import { ResponseUploadPhoto, User, UserAP } from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {


  private readonly baseApi = "http://localhost:8080"
  // private baseApiPhoto = "https://1cfe-125-164-20-251.ap.ngrok.io"

  constructor(private httpClient: HttpClient) { }

  getAPUsers(): Observable<UserAP[]> {

    // const token = localStorage.getItem('token')

    // const header = new HttpHeaders({
    //   'Authorization': `Bearer ${token}`
    // })

    // return this.httpClient.get<ResponseBase<UserAP[]>>(this.baseApi + "/nabung_bareng/view_all_users", {headers: header}).pipe(
    return this.httpClient.get<ResponseBase<UserAP[]>>(this.baseApi + "/nabung_bareng/view_all_users").pipe(
      map((val) => {
        console.log(val)
        const data: UserAP[] = [];
          // mapping data for new response
          for (let item of val.data) {
            data.push({
              id: item.id,
              username: item.username,
              name: item.name,
              email: item.email,
              dob: item.dob,
              photo: `${this.baseApi}/nabung_bareng/files/${item.photo}:.+`,
              wallet: {
                  saldo: item.wallet.saldo
              },
              role: {
                  role: item.role.role
              },
              memberList: item.memberList
            });
          }
          return data;
      }),
      catchError((err) => {
        console.log(err)
        throw err
      })
    )


  }

  uploadPhoto(data: File): Observable<ResponseBase<string>> {
    // const token = localStorage.getItem('token')

    // const header = new HttpHeaders({
    //   'Authorization': `Bearer ${token}`
    // })

    const file = new FormData()
    file.append('file', data, data.name)
    // return this.httpClient.post<ResponseBase<string>>(`${this.baseApi}/nabung_bareng/upload_file`, file, {headers: header})
    return this.httpClient.post<ResponseBase<string>>(`${this.baseApi}/nabung_bareng/upload_file`, file)
  }

  addUser(payload: any) {
    return this.httpClient.post<ResponseBase<any>>(`${this.baseApi}/register`, payload)
  }


}
