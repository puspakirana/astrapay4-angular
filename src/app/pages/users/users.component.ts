import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user.interface';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent {
  userList!: User[]

  constructor(private dataService: DataService) {
    this.dataService.getUser().subscribe(
      response => {
        this.userList = response
        console.log(response)
      }
    )
   }

}

// import { Component, OnInit } from '@angular/core';
// import { User, UserAP } from 'src/app/interfaces/user.interface';
// import { DataService } from 'src/app/services/data.service';
// import { UserService } from 'src/app/services/user.service';

// @Component({
//   selector: 'app-users',
//   templateUrl: './users.component.html',
//   styleUrls: ['./users.component.scss']
// })
// export class UsersComponent {
//   user: UserAP[] = []

//   constructor(private userService: UserService) {
//     this.userService.getAPUsers().subscribe(
//       response => {
//         console.log(response)
//         this.user = response
//       }
//     )
//    }

// }



