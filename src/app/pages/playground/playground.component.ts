import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss']
})
export class PlaygroundComponent {

  title = 'astrapay4-angular';

  name = 'Default'
  age = 1
  status = false

  showData = true

  nomor = 6

  person = {
    title: 'Test A',
    name: 'FEFE',
    age: 22,
    status: true
  }

  personList = [
    {
    title: 'Test A',
    name: 'FEFE',
    age: 22,
    status: true
    },
    {
      title: 'Test B',
      name: 'GILANG',
      age: 26,
      status: true
    },
    {
      title: 'Test C',
      name: 'IMRUN',
      age: 23,
      status: false
    }
]

  datas = [1, 2, 3]

  constructor() {
    this.name = 'Puspakirana'
    this.age = 22
  }

  onCallBack(ev: any) {
    console.log(ev)
  }

  onCallBackAdd(ev: any){
    ev.data.title = "Ini tambahan aja sih"
    this.personList.push(ev.data)
    console.log(this.personList)
  }

  onCallBackChoose(ev: any){
    console.log(ev)
  }

}
