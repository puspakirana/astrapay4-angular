import { Component, OnInit, TemplateRef } from '@angular/core';
import { UserAP } from 'src/app/interfaces/user.interface';
import { UserService } from 'src/app/services/user.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { map, Subject, switchMap, takeUntil } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent{

  modalRef?: BsModalRef;
  user: UserAP[] = []

  photo!: string
  photoFile!: File

  refresh = new Subject<void>();

  constructor(private userService: UserService, private modalService: BsModalService, private authService: AuthService) {
    this.userService.getAPUsers().subscribe(
      response => {
        console.log(response)
        this.user = response
      }
    );
    this.loadData();
   }

   openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  showPreview(event: any){
    if(event){
      const file = event.target.files[0]
      this.photoFile = file
      const reader = new FileReader();
      reader.onload = () => {
        this.photo = reader.result as string
      }
      reader.readAsDataURL(file);
    }
  }

  doAddUser() {
    this.userService.uploadPhoto(this.photoFile).pipe(
      switchMap(val => {

        const payload = {
          username: "1234567",
          password: "password",
          confirm_password: "password",
          email: "test3@gmail.com",
          name: "test3",
          dob: "2022-11-01",
          photo: val.data
        }

        return this.authService.register(payload).pipe(
          map(val => val)
        )
      })
    ).subscribe( response => console.log(response))
  }

  loadData() {
    this.userService
      .getAPUsers()
      .pipe(takeUntil(this.refresh))
      .subscribe((response) => {
        console.log(response);
        this.user = response;
      });

  }



}
