import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { RouterModule, Routes } from '@angular/router';
import { LayoutsModule } from 'src/app/layouts/layouts.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SettingsComponent } from './settings/settings.component';
import { GroupListComponent } from './group-list/group-list.component';

const routes: Routes = [
  // {
  //   path: '',
  //   component: AdminComponent,
  //   children: [
  //     {
  //       path: 'dashboard',
  //       component: DashboardComponent
  //     },
  //     {
  //       path: 'settings',
  //       component: SettingsComponent
  //     }
  //   ]
  // }
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'settings',
    component: SettingsComponent
  },
  {
    path: 'group-list',
    component: GroupListComponent
  }
]

@NgModule({
  declarations: [AdminComponent, DashboardComponent, SettingsComponent, GroupListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    LayoutsModule
  ]
})
export class AdminModule { }
