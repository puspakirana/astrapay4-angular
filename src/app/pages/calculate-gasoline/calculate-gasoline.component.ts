import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-calculate-gasoline',
  templateUrl: './calculate-gasoline.component.html',
  styleUrls: ['./calculate-gasoline.component.scss']
})
export class CalculateGasolineComponent {

  
  bensin = "?"
  harga = 0
  total = 0
  change = 0
  status = "(klik tombol hitung untuk memproses)"
  showChange = false

  constructor(private dataService: DataService) {
    this.harga = this.dataService.baseHarga
  }


  testClass = "alert alert-success"

  @Input() jumlahLiter = 0
  @Input() jumlahBayar = 0

  @Output() dataCallBack = new EventEmitter()

  cekHarga(value: any) {
    if(value == 'Pertamax'){
      this.harga = 1000
      this.bensin = "Pertamax"
    } else if (value == 'Pertalite'){
      this.harga = 500
      this.bensin = "Pertalite"
    } else if (value == 'Solar') {
      this.harga = 1500
      this.bensin = "Solar"
    } else if (value == 'PertamaxPlus'){
      this.harga = 1800
      this.bensin = "Pertamax Plus"
    } else {
      this.harga = 0
      this.bensin = "Jenis Bensin Tidak Ditemukan"
    }
  }

  hitungYuk(liter: number, bayar: number){

    if(this.bensin == "?"){
      this.status = "PILIH BENSIN DULU WOI!"
    } else {
      this.total = this.harga * liter
      if(this.total > bayar){
        this.status = "GAGAL! Uang Anda kurang!!"
        this.showChange = false
      } else {
        this.status = "BERHASIL"
          this.change = bayar - this.total
          this.showChange = true
      }
      
    }
  }

}
