import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  name = ""
  email = ""
  username = ""
  password = ""
  dob = ""

  formRegister: FormGroup

  constructor(private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder){
      this.formRegister = this.formBuilder.group({
        username: ['', [Validators.required]],
        name: ['', [Validators.required]],
        email: ['', [Validators.required, Validators.email]],
        dob: ['', [Validators.required]],
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirm_password: ['', [Validators.required]]
      })
    }

    get errorControl(){
      return this.formRegister.controls;
    }

    get confirmPassword(){
      return this.formRegister.value.password == this.formRegister.value.confirm_password
    }

  doRegister() {
    const payload = {
      username: this.formRegister.value.username,
      password: this.formRegister.value.password,
      confirm_password: this.formRegister.value.confirm_password,
      email: this.formRegister.value.email,
      name: this.formRegister.value.name,
      dob: this.formRegister.value.dob
    }

    this.authService.register(payload).subscribe(
      response => {
        alert("Registered!")
        this.router.navigate(['/playground'])
      }, error => {
        console.log(error)
        alert(error.error.message)
      }
    )

  }

  // doLogin() {

  //   const payload = {
  //     username: this.username,
  //     password: this.password
  //   }

  //   this.authService.login(payload).subscribe(
  //     response => {
  //       console.log(response)
  //     }
  //   )
  // }
}
