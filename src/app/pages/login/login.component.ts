import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent{

  username = ""
  password = ""

  formLogin: FormGroup

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder){
      this.formLogin = this.formBuilder.group({
        username: ['', [Validators.required]],
        password: ['', [Validators.required, Validators.minLength(6)]]
      })
    }

    get errorControl(){
      return this.formLogin.controls;
    }
    

  doLogin() {

    console.log(this.formLogin)

    const payload = {
      username: this.formLogin.value.username,
      password: this.formLogin.value.password
    }

    this.authService.login(payload).subscribe(
      response => {
        console.log(response)
        // if(response.token) {
        //   this.router.navigate(['/playground'])
        // }
        Swal.fire({
          title: 'Login Success!',
          text: 'You are logged as admin',
          icon: 'success',
          confirmButtonText: 'Cool'
        })
        // alert(response.data.role)
        if(response.data.role === 'ROLE_ADMIN'){
          localStorage.setItem('token', response.data.token)
          this.router.navigate(['/admin/dashboard'])
        } else if(response.data.role === 'ROLE_USER') {
          this.router.navigate(['/users'])
        } else {
          this.router.navigate(['/playground'])
        }
      }, error => {
        console.log(error)
        alert(error.error.message)
      }
    )
  }

}
