export interface User {
    id: number,
    name: string,
    phone: string,
    username: string,
    website: string,
    email: string,
    company: {
      bs: string,
      catchPhrase: string,
      name: string
    },
    address: {
      city: string,
      street: string,
      suite: string,
      zipcode: string,
      geo: {
        lat: string,
        lng: string
      }
    }
  }

  export interface UserAP {
    id: number,
    username: string,
    name: string,
    email: string,
    dob: string,
    photo?: string,
    wallet: {
        saldo: number
    },
    role: {
        role: string
    },
    memberList: []
  }

  export interface ResponseUploadPhoto {
    image: string
  }